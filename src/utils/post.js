import http from './http.js'
import qs from 'qs'
let baseUrl = (process.env.NODE_ENV == 'development' ? 'http://124.70.151.37:20001' :'http://124.70.151.37:20001')
let timer = null;
export function post(url,data){
        return new Promise((resolve,reject)=>{
            http.post(baseUrl+url,data).then(res=>{
                resolve(res)
            }).catch(res=>{
                reject(res)
            })
           }) 
}

export function get(url,data){
    return new Promise((resolve,reject)=>{
        http.request({
            url:baseUrl + url,
            method:'post',
            params: data,
        }).then(res=>{
            resolve(res)
        })
       }) 
 }

export function request(url,data) {
     let value;
     return (async ()=>{
         let datas = await get(url,data);
         value = datas;
         return value
     })()
 }