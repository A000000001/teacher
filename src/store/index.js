import Vue from 'vue'
import Vuex from 'vuex'

import createPersistedState from "vuex-persistedstate"
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    active:'',
    isIosMoveBack: false
  },
  mutations: {
    changeActive(state,value){
      state.active = value;
    },
    setIsIosMoveBack (state, param) {
      state.isIosMoveBack = param
  }
  },
  actions: {
  },
  modules: {
  },
  // plugins: [createPersistedState()]
})
