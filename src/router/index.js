import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  // {
  //   path: '/about',
  //   name: 'About',
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // }
  {
    path: '/dashborad',
    name: 'dashborad',
    redirect: '/dashborad/home',
    component: () => import(/* webpackChunkName: "about" */ '../views/Dashborad.vue'),
    children: [
      {
        path: '/dashborad/home',
        name: 'home',
        meta: { index: 1 },
        component: () => import(/* webpackChunkName: "about" */ '../views/home/Home.vue'),
      },
      {
        path: '/dashborad/lesson',
        name: 'lesson',
        meta: { index: 1 },
        component: () => import(/* webpackChunkName: "about" */ '../views/lesson/Lesson.vue'),
      },
      {
        path: '/dashborad/cycle',
        name: 'cycle',
        meta: { index: 1 },
        component: () => import(/* webpackChunkName: "about" */ '../views/cycle/Cycle.vue'),
      },
      {
        path: '/dashborad/my',
        name: 'my',
        meta: { index: 1 },
        component: () => import(/* webpackChunkName: "about" */ '../views/my/My.vue'),
      }
    ]
  },
  {
    path: '/check',
    meta:{index:2},
    component: () => import(/* webpackChunkName: "about" */ '../views/check/Checkout.vue'),
  },
  {
    path: '/list',
    meta:{index:2},
    component: () => import(/* webpackChunkName: "about" */ '../views/distribution/List.vue'),
  },
  {
    path: '/lessonapprove',
    meta:{index:3},
    component: () => import(/* webpackChunkName: "about" */ '../views/approver/Lessonapprove.vue'),
  },
  {
    path: '/lessondel',
    meta:{index:3},
    component: () => import(/* webpackChunkName: "about" */ '../views/approver/Dellesson.vue'),
  },
  {
    path: '/addpeople',
    meta:{index:3},
    component: () => import(/* webpackChunkName: "about" */ '../views/approver/Addpeople.vue'),
  },
  {
    path: '/delpeople',
    meta:{index:3},
    component: () => import(/* webpackChunkName: "about" */ '../views/approver/Delpeople.vue'),
  },
  {
    path: '/dispose',
    meta:{index:3},
    component: () => import(/* webpackChunkName: "about" */ '../views/approver/myapprove/Dispose.vue'),
  },
  {
    path: '/manage',
    meta:{index:3},
    component: () => import(/* webpackChunkName: "about" */ '../views/approver/myapprove/Manage.vue'),
  },
  {
    path: '/sponsor',
    meta:{index:3},
    component: () => import(/* webpackChunkName: "about" */ '../views/approver/myapprove/Sponsor.vue'),
  },
  {
    path: '/disposedetail',
    meta:{index:4},
    component: () => import(/* webpackChunkName: "about" */ '../views/approver/myapprove/DisposeDetail.vue'),
  },
  {
    path: '/approvehome',
    meta:{index:2},
    component: () => import(/* webpackChunkName: "about" */ '../views/approver/Approvehome.vue'),
  },
  {
    path: '/dispeople',
    meta:{index:3},
    component: () => import(/* webpackChunkName: "about" */ '../views/distribution/Dispeople.vue'),
  },
  {
    path: '/role',
    meta:{index:4},
    component: () => import(/* webpackChunkName: "about" */ '../views/distribution/Role.vue'),
  },
  {
    path: '/changepart',
    meta:{index:4},
    component: () => import(/* webpackChunkName: "about" */ '../views/distribution/Changepart.vue'),
  },
  {
    path: '/lessonlist',
    meta:{index:2},
    component: () => import(/* webpackChunkName: "about" */ '../views/lesson/list/Lessonlist.vue'),
  },
  {
    path: '/details',
    meta:{index:3},
    component: () => import(/* webpackChunkName: "about" */ '../views/lesson/list/Details.vue'),
  },
  {
    path: '/pdf',
    meta:{index:4},
    component: () => import(/* webpackChunkName: "about" */ '../views/lesson/list/Pdf.vue'),
  },
  {
    path: '/video',
    meta:{index:4},
    component: () => import(/* webpackChunkName: "about" */ '../views/lesson/list/Video.vue'),
  },
  {
    path: '/extravideo',
    meta:{index:4},
    component: () => import(/* webpackChunkName: "about" */ '../views/lesson/list/Extravideo.vue'),
  },
  {
    path: '/approver',
    meta:{index:5},
    component: () => import(/* webpackChunkName: "about" */ '../views/lesson/list/Approver.vue'),
  },
  {
    path: '/login',
    meta:{index:0},
    component: () => import(/* webpackChunkName: "about" */ '../views/login/Login.vue'),
  },





  // 教研圈列表
  {
    path: '/circleList',
    meta: { index: 2 },
    component: () => import(/* webpackChunkName: "about" */ '../views/cycle/list/circleList.vue'),
  },
  // 与我相关 relatedToMe
  {
    path: '/relatedToMe',
    meta: { index: 3 },
    component: () => import(/* webpackChunkName: "about" */ '../views/cycle/relatedToMe/relatedToMe.vue'),
  },
  // 个人展示页面
  {
    path: '/Personal',
    meta: { index: 4 },
    component: () => import(/* webpackChunkName: "about" */ '../views/cycle/personal/Personal.vue'),
  },
  // 个人信息页面
  {
    path: '/userInfo',
    meta: { index: 2 },
    component: () => import(/* webpackChunkName: "about" */ '../views/my/userInfo/userInfo.vue'),
  },
  // 个性签名 Signature
  {
    path: '/Signature',
    meta: { index: 3 },
    component: () => import(/* webpackChunkName: "about" */ '../views/my/signature/Signature.vue'),
  },
  // 奖章详情
  {
    path: '/medalDetail',
    meta: { index: 2 },
    component: () => import(/* webpackChunkName: "about" */ '../views/my/medalDetail/medalDetail.vue'),
  },
  // 备课视频任务  videoTask
  {
    path: '/videoTask',
    meta: { index: 2 },
    component: () => import(/* webpackChunkName: "about" */ '../views/my/videoTask/videoTask.vue'),
  },
  // 视频待审批详情
  {
    path: '/pendingDetail',
    meta: { index: 3 },
    component: () => import(/* webpackChunkName: "about" */ '../views/my/pendingDetail/pendingDetail.vue'),
  },
  // 视频文件上传
  {
    path: '/uploadFile',
    meta: { index: 4 },
    component: () => import(/* webpackChunkName: "about" */ '../views/my/uploadFile/uploadFile.vue'),
  },
  // 图片文件上传
  {
    path: '/uploadImgFile',
    meta: { index: 4 },
    component: () => import(/* webpackChunkName: "about" */ '../views/my/uploadImgFile/uploadImgFile.vue'),
  },
  // 手写教案任务 handleTask
  {
    path: '/handleTask',
    meta: { index: 2 },
    component: () => import(/* webpackChunkName: "about" */ '../views/my/handleTask/handleTask.vue'),
  },
  // 教室图片任务 roomTask
  {
    path: '/roomTask',
    meta: { index: 2 },
    component: () => import(/* webpackChunkName: "about" */ '../views/my/roomTask/roomTask.vue'),
  },
  // 精彩视频审批  videoApproval
  {
    path: '/videoApproval',
    meta: { index: 2 },
    component: () => import(/* webpackChunkName: "about" */ '../views/my/videoApproval/videoApproval.vue'),
  },
  // 精彩视频审批待处理  toBeProcessed
  {
    path: '/toBeProcessed',
    meta: { index: 3 },
    component: () => import(/* webpackChunkName: "about" */ '../views/my/toBeProcessed/toBeProcessed.vue'),
  },
  // 审批详情 processedDetail
  {
    path: '/processedDetail',
    meta: { index: 4 },
    component: () => import(/* webpackChunkName: "about" */ '../views/my/processedDetail/processedDetail.vue'),
  },
  // 消息
  {
    path: '/Message',
    meta: { index: 2 },
    component: () => import(/* webpackChunkName: "about" */ '../views/my/message/Message.vue'),
  },
   // 精彩视频审批  videoApproval
   {
    path: '/opations',
    meta: { index: 2 },
    component: () => import(/* webpackChunkName: "about" */ '../views/my/opations/Opations.vue'),
  },
  // 消息详情
  {
    path: '/msgDetails',
    meta: { index: 3 },
    component: () => import(/* webpackChunkName: "about" */ '../views/my/msgDetails/msgDetails.vue'),
  },
  // 我的积分  Integral
  {
    path: '/Integral',
    meta: { index: 2 },
    component: () => import(/* webpackChunkName: "about" */ '../views/my/integral/Integral.vue'),
  },
  // 积分规则 pointsRules
  {
    path: '/pointsRules',
    meta: { index: 3 },
    component: () => import(/* webpackChunkName: "about" */ '../views/my/pointsRules/pointsRules.vue'),
  },
  // 积分明细 pointsDetails
  {
    path: '/pointsDetails',
    meta: { index: 3 },
    component: () => import(/* webpackChunkName: "about" */ '../views/my/pointsDetails/pointsDetails.vue'),
  },
  // 设置
  {
    path: '/Setting',
    meta: { index: 2 },
    component: () => import(/* webpackChunkName: "about" */ '../views/my/setting/Setting.vue'),
  },
  // 版本号
  {
    path: '/Version',
    meta: { index: 3 },
    component: () => import(/* webpackChunkName: "about" */ '../views/my/setting/Version/Version.vue'),
  },
  // 关于我们 aboutUs
  {
    path: '/aboutUs',
    meta: { index: 3 },
    component: () => import(/* webpackChunkName: "about" */ '../views/my/setting/aboutUs/aboutUs.vue'),
  },
  // 修改密码
  {
    path: '/changePassword',
    meta: { index: 3 },
    component: () => import(/* webpackChunkName: "about" */ '../views/my/setting/changePassword/changePassword.vue'),
  },
  // 更多评论
  {
    path: '/MoreCom',
    meta: { index: 4 },
    component: () => import(/* webpackChunkName: "about" */ '../views/cycle/MoreCom/MoreCom.vue'),
  },
  {
    path: '/',
    redirect:'/dashborad'
  },
  {
    path: '*',
    redirect:'/dashborad'
  },
]

const router = new VueRouter({
  routes
})
// 登录状态验证
router.beforeEach((to,from,next)=>{
  if(to.path == '/login') return next();
  const token = localStorage.getItem('token');
  if(token) return next();
  router.push('/login'); 
})
// 解决重复点击报错
const originalPush = VueRouter.prototype.push
   VueRouter.prototype.push = function push(location) {
   return originalPush.call(this, location).catch(err => err)
}

export default router
