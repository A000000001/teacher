import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import Vant from 'vant';
import { post, get, request } from './utils/post.js'
import 'vant/lib/index.css';
import { Toast } from 'vant';
import JSEncrypt from 'jsencrypt';
import dd from 'dingtalk-jsapi'
import VideoPlayer from 'vue-video-player'
import Videojs from 'video.js'
import 'video.js/dist/video-js.css'
import FastClick from 'fastclick'
import { Lazyload } from 'vant';
FastClick.attach(document.body);

require('video.js/dist/video-js.css')
require('vue-video-player/src/custom-theme.css')
Vue.use(Lazyload);
Vue.use(Lazyload, {
  lazyComponent: true,
});
Vue.prototype.$video = Videojs
Vue.use(VideoPlayer)
dd.ui.webViewBounce.disable()
Vue.prototype.post = post;
Vue.prototype.get = get;
Vue.prototype.request = request;
Vue.use(Vant);
Vue.config.productionTip = false
Vue.prototype.Toast = Toast
Vue.prototype.$getRsaCode = function (str) { // 注册方法
  let pubKey = 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCCaGn5+Yq9o7kxmDS2zwIREIEkcpKbx44uPT/PX22XOjgzkMvbCEamu7KyatNhkrrSH6E14l1F739WztXffJ/OCXUmgF4egMedyGCA8ymZWbzyhwnZcywa8me8S2rjb3gkCD6VFq8BSPJx9vaMvlR7eZxe1O5lllaV6tE6SlNY7QIDAQAB';// ES6 模板字符串 引用 rsa 公钥
  let encryptStr = new JSEncrypt();
  encryptStr.setPublicKey(pubKey); // 设置 加密公钥
  let data = encryptStr.encrypt(str.toString());  // 进行加密
  return data;
}
dd.biz.navigation.setLeft({
  show: true, //控制按钮显示
  control: true,//是否控制点击事件
  text: '返回',//控制显示文本
  onSuccess : function(result) {
  //返回上一页所在的内容
  window.history.go(-1); 
  },
  onFail:function(){}
});
// 钉钉客户端进入
dd.biz.navigation.setRight({
  show: false, //控制按钮显示， true 显示， false 隐藏， 默认true
})
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
