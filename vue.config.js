module.exports = {
    devServer:{
        disableHostCheck:true
    },
    lintOnSave: false,
    css: {
        loaderOptions: {
        postcss: {
            plugins: [
            require("autoprefixer")({
                // 配置使用 autoprefixer
                overrideBrowserslist: ["last 15 versions"],
                browsers: ["Android >= 4.0", "iOS >= 7"]
            }),
            require("postcss-pxtorem")({
                rootValue: 50, // 换算的 基数
                propList: ["*"]
            })
            ]
        }
        }
    }
}  